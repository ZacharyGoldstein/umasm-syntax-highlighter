UMASM Syntax Highlighters


Author:        Zachary Goldstein (zgolds01), Tufts University
Created:       23 Apr 2020
Last Modified: 30 Nov 2020


A note from the author:

    The inspiration for this package was my final project for COMP 0040 (in
    the spring semester of 2020), the RPN calculator.

    Designing an RPN calculator in an assembly-level language is an interesting
    challenge, but I found that programming without UMASM specific syntax
    highlighting was often frustrating.

    After attempting to program in UMASM for a few minutes, I decided that I
    could not bring myself to work with unformatted, not-at-all-beautified,
    plain-text code. Since I have a particularly profound proclivity for
    productive procrastination, I spent some time implementing a UMASM syntax
    highlighter for Sublime 3.

    I implemented two versions:
        1) The first is simpler. It will style any .ums file using a color
           scheme similar to Sublime's default C highlighter.
        2) The second includes additional regex that will identify and
           brightly highlight ill-formed code.

    Since the implementation of the original syntax highlighter for Sublime 3,
    support has (maybe) been added for other popular text editors.

    